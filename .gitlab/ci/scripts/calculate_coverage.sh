#!/usr/bin/env sh

filter () {
  jq -cjM '[.total[].pct]? | add / length' 2>/dev/null
  return $?
}

if [[ -p /dev/stdin ]]; then
  filter
  exit $?
fi
if [[ -f "${1}" ]]; then
  cat "${1}" | filter
  exit $?
fi

exit 1
