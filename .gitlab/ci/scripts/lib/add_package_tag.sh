#This script is meant to be sourced!

add_package_tag () {
  [[ -z "${1}" -o -z "${2}" ]] && return 1
  local tag="${2}"
  local version="${1}"

  npm dist-tag add "@${CI_PROJECT_PATH}@${version}" "${tag}"

  return $?
}
