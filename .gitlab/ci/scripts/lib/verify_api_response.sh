#This script is meant to be sourced!

verify_api_response () {
  filter () {
    jq -cjM 'select(.error? or .message?)[] // empty' 2>/dev/null
    return $?
  }

  if [[ -p /dev/stdin ]]; then
    filter
    return $?
  fi
  if [[ -f "${1}" ]]; then
    cat "${1}" | filter
    return $?
  fi
  if [[ -n "${1}" ]]; then
    printf '%s' "${1}" | filter
    return $?
  fi

  return 1
}
