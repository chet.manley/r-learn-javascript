#This script is meant to be sourced!

update_total_pages () {
  [[ -n "${1}" && -n "${2}" ]] || return 1
  local headers_file="${1}"
  local new_total_pages=''
  local total_pages="${2}"

  if [[ -f "${headers_file}" ]]; then
    new_total_pages=$(sed -n 's/^x-total-pages:\s\+\([0-9]\+\)/\1/p' ${headers_file})
  fi

  printf '%s' "${new_total_pages:-${total_pages}}"

  return 0
}
