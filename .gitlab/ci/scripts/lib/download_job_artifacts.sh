#This script is meant to be sourced!

# `lib` must be set in sourcing script
. "${lib}/api_download.sh"

download_job_artifacts () {
  [[ -n "${1}" ]] || return 1
  local job_id="${1}"
  local ec=0
  local error=''
  local filepath="/tmp/${CI_JOB_ID}-archive.zip"

  error=$(api_download "jobs/${job_id}/artifacts" "${filepath}")
  ec=$?
  if [[ $ec -ne 0 ]]; then
    printf '%s' "${error}"
    return $ec
  fi

  printf '%s' "${filepath}"

  return 0
}
