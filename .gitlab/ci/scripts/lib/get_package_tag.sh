#This script is meant to be sourced!

# `lib` must be set in sourcing script
. "${lib}/get_package_version.sh"
. "${lib}/get_published_tags.sh"

get_package_tag () {
  local ec=0
  local latest=''
  local major=''
  local prerelease=''
  local tag=''
  local version=''

  version=$(get_package_version)
  ec=$?
  if [[ $ec -ne 0 ]]; then
    echo 'Failed to retrieve package version.' 1>/dev/stderr
    return $ec
  fi

  prerelease=$( \
    printf '%s' "${version}" \
    | jq -cjMR '[splits("[-.]")][3] // empty' \
  )

  if [[ "${CI_COMMIT_BRANCH}" == "${CI_DEFAULT_BRANCH}" ]]; then
    # safeguard against publishing prerelease versions from master
    if [[ -n "${prerelease}" ]]; then
      echo "Committing prerelease versions to ${CI_DEFAULT_BRANCH} is prohibited." 1>/dev/stderr
      return 1
    fi
    tag='latest'
  elif [[ -n "${CI_COMMIT_TAG}" -a "${CI_COMMIT_TAG}" != "v${version}" ]]; then
    echo "Commit tag [${CI_COMMIT_TAG}] must match package version [v${version}]." 1>/dev/stderr
    return 1
  elif [[ -z "${prerelease}" ]]; then
    latest=$(get_published_tags latest | jq -cjMR 'split(" ")[1]')
    # ignore ec to prevent first-publish from failing

    if [[ "${latest}" \< "${version}" ]]; then
      tag='latest'
    else
      major=$(printf '%s' "${version}" | jq -cjMR 'split(".")[0]')
      tag="latest-v${major}"
    fi
  else
    tag="${prerelease:-next}"
  fi

  printf '%s' "${tag}"

  return 0
}
