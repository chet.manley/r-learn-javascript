#!/usr/bin/env sh

# Script must always be run from project root.
cd "${CI_PROJECT_DIR}"
lib=$(readlink -f '.gitlab/ci/scripts/lib/')

. "${lib}/get_package_tag.sh"

pkg_tag=$(get_package_tag)
ec=$?
if [[ $ec -ne 0 ]]; then
  echo 'Failed to retrieve package tag.' 1>/dev/stderr
  exit $ec
fi

npm publish --tag "${pkg_tag}"

exit $?
