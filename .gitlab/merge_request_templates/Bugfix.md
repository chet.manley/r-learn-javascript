<!--
> Title(above):
> First word should be an imperative, present tense verb.
> E.g., "Fix", not "Fixed" or "Fixes".
> Rest of Title should be very concise and contextual.
> E.g., Fix result parsing for unintended dataset properties
-->

# Context

<!--
> Provide context for the issue this change intends to resolve.
-->

## Code Detail Summary

<!--
> Include a detailed description of each code change,
> along with associated reasoning for the change.
> E.g.:
1. Add explicit return value from `Class.privateGetMethod`.
  - Implicit return values can have unexpected consequences.
  - Return `null` when requested property is not defined.
1. Add explicit null check when calling `Class.privateGetMethod()`.
  - Removes possible ambiguity between empty and undefined properties.
  - Faciliates more precise error handling.
1. ...
-->

## New Tests

<!--
> Include a brief description of the tests added
> to prevent this bug from recurring in the future.
> E.g.:
- Ensure `Class.privateGetMethod` reliably returns expected output.
  - Full suite of synthetic valid and invalid inputs.
  - Includes a mock production dataset.
- ...
-->

Resolves #<issue_ref>
/label ~bugfix

<!--
> Required: replace <issue_ref> with the issue number(s) this merge resolves.
-->
