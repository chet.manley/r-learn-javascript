<!--
> Title(above):
> First word should be an imperative, present tense verb.
> E.g., "Add", not "Added" or "Adds".
> Rest of Title should be very concise and contextual.
> E.g., Add new API discovery endpoint to /api
-->

# Context

<!--
> Provide context for the feature this change intends to implement.
-->

## Code Detail Summary

<!--
> Include a detailed description of each code change,
> along with associated reasoning for the change.
> E.g.:
1. Export `ExtensionClass` for public consumption.
  - Provides well-defined interface for building extensions.
  - Allows read access to sanitized dataset for arbitrary use.
1. Add `Class.addExtension` public method.
  - Allows users to extend inbuilt functionality.
1. Add `Class.extensions` property.
  - Stores user-added extensions.
1. ...
-->

## New Tests

<!--
> Include a brief description of the tests added
> to help mitigate introducing future bugs.
> E.g.:
- Ensure `ExtensionClass` is publically available.
  - Required for users to build their own extensions.
- Ensure all `Class.extensions` are loaded on every run.
  - ...
- ...
-->

## New Documentation

<!--
> If applicable, include details for any new documentation that was added.
-->

Resolves #<issue_ref>
/label ~feature

<!--
> Required: replace <issue_ref> with the issue number(s) this merge resolves.
-->
