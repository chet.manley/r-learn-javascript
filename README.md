# r-learn-javascript

> **An archive of small projects I have used as examples in r/learnjavascript.**

[![code style standardjs][standardjs-badge]][standardjs-url]
[![versioning strategy][semver-badge]][semver-url]

[![CI pipeline status][ci-badge]][ci-url]
[![code coverage][coverage-badge]][coverage-url]

<!-- badge images and URLs -->
[ci-badge]: https://gitlab.com/chet.manley/r-learn-javascript/badges/master/pipeline.svg
[ci-url]: https://gitlab.com/chet.manley/r-learn-javascript
[coverage-badge]: https://gitlab.com/chet.manley/r-learn-javascript/badges/master/coverage.svg?job=Code%20Coverage
[coverage-url]: https://chet.manley.gitlab.io/r-learn-javascript/master/coverage
[semver-badge]: https://img.shields.io/static/v1?label=semver&message=standard-version&color=brightgreen&logo=&style=for-the-badge
[semver-url]: https://github.com/conventional-changelog/standard-version
[standardjs-badge]: https://img.shields.io/static/v1?label=style&message=standardJS&color=brightgreen&logo=&style=for-the-badge
[standardjs-url]: https://standardjs.com/

---

## Built with

[![Fedora Linux](https://img.shields.io/static/v1?logo=fedora&label=Fedora&message=workstation&color=294172&style=flat)](https://getfedora.org/)
[![VSCode](https://img.shields.io/static/v1?logo=visual-studio-code&label=VSCode&message=insiders&color=2A917D&style=flat)](https://code.visualstudio.com/)
[![GitLab](https://img.shields.io/static/v1?logo=gitlab&label=GitLab&message=FOSS&color=DE4020&style=flat)](https://gitlab.com/gitlab-org/gitlab)
[![Caffeine](https://img.shields.io/static/v1?logo=buy-me-a-coffee&label=Caffeine&message=☕&color=603015&style=flat)](https://en.wikipedia.org/wiki/Caffeine)

## Contributing

The r/learnjavascript community is welcome to participate in this open source project.
Aspiring contributors should review the [contributing guide](https://gitlab.com/chet.manley/r-learn-javascript/-/blob/master/CONTRIBUTING.md) for details on how to get started.

## License

[![NPM][license-badge]][license-url]

Copyright © 2020 [Chet Manley](https://gitlab.com/chet.manley).

<!-- badge images and URLs -->
[license-badge]: https://img.shields.io/badge/license-MIT-green?style=flat-square
[license-url]: https://gitlab.com/chet.manley/r-learn-javascript/-/blob/master/LICENSE
